#pragma once
#define MAP_MOVE_TOP		(0)
#define MAP_MOVE_LEFT		(0)
#define MAP_MOVE_RIGHT		(6400)
#define MAP_MOVE_BOTTOM		(6400)

#define TILE_SIZE			(64)
#define TILE_CENTER_X		(32)
#define TILE_CENTER_Y		(64)

#define CHARACTER_CENTER_X	(71)
#define CHARACTER_CENTER_Y	(93)
#define EFFECT_CENTER_X		(70)
#define EFFECT_CENTER_Y		(70)
#define HPGAUAGE_CENTER_X	(35)
#define HPGAUAGE_CENTER_Y	(7)
#define SHADOW_CENTER_X		(32)
#define SHADOW_CENTER_Y		(9)

#define MOVE_DISTANCE_X		(3)
#define MOVE_DISTANCE_Y		(2)

#define ACTION_MOVE_LL		(0)
#define ACTION_MOVE_LU		(1)
#define ACTION_MOVE_UU		(2)
#define ACTION_MOVE_RU		(3)
#define ACTION_MOVE_RR		(4)
#define ACTION_MOVE_RD		(5)
#define ACTION_MOVE_DD		(6)
#define ACTION_MOVE_LD		(7)

#define ACTION_ATTACK1		(11)
#define ACTION_ATTACK2		(12)
#define ACTION_ATTACK3		(13)

#define ACTION_STAND		(255)

#define DELAY_STAND			(5)
#define DELAY_MOVE			(4)
#define DELAY_ATTACK1		(3)
#define DELAY_ATTACK2		(4)
#define DELAY_ATTACK3		(4)
#define DELAY_EFFECT		(3)

#define CLIENT_COUNT_MAX	(2000)
#define CLIENT_NOT_USED		(-1)
#define CLIENT_CLOSED		(-2)
#define BUF_SIZE			(1024)
#define SEND_QUEUE_SIZE		(2049)
#define RECV_QUEUE_SIZE		(2049)

#define SECTOR_SIZE			(200)

