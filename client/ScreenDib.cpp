#include "stdafx.h"
#include "ScreenDib.h"

ScreenDib::ScreenDib(int width, int height, int color_bit)
	:_width(width), _height(height), _color_bit(color_bit)
{
	int pitch = _width * color_bit / 8;
	pitch = (pitch + 3) & ~3;
	_pitch = pitch;

	CreateDibBuffer(_width, _height, _color_bit);
}

ScreenDib::~ScreenDib()
{

}

void ScreenDib::DrawBuffer(HWND wnd, int x, int y)
{
	HDC dc = GetDC(wnd);

	StretchDIBits(dc, x, y, _pitch, _height, 0, 0, _pitch, _height, _buffer, (BITMAPINFO*)&_dib_info, DIB_RGB_COLORS, SRCCOPY);

	DeleteDC(dc);
}

BYTE* ScreenDib::GetDibBuffer()
{
	return _buffer;
}

int ScreenDib::GetWidth()
{
	return _width;
}

int ScreenDib::GetHeight()
{
	return _height;
}

int ScreenDib::GetPtich()
{
	return _pitch;
}

void ScreenDib::CreateDibBuffer(int width, int height, int color_bit)
{
	BITMAPINFOHEADER bitmap_info_header;
	bitmap_info_header.biSize = sizeof(BITMAPINFOHEADER);
	bitmap_info_header.biWidth = width;
	bitmap_info_header.biHeight = -1*height;
	bitmap_info_header.biPlanes = 1;
	bitmap_info_header.biBitCount = color_bit;
	bitmap_info_header.biCompression = 0;
	bitmap_info_header.biSizeImage = _pitch * height;
	bitmap_info_header.biXPelsPerMeter = 0;
	bitmap_info_header.biYPelsPerMeter = 0;
	bitmap_info_header.biClrUsed = 0;
	bitmap_info_header.biClrImportant = 0;

	_dib_info = *(BITMAPINFO*)&bitmap_info_header;
	
	_buffer = new BYTE[height * width * color_bit / 8];
}

void ScreenDib::ReleaseDibBuffer()
{
	delete[] _buffer;
}
