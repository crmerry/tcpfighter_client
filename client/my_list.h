// ListWithClass.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.


/*
	head <-> tail
	head <-> n1 <-> n2 <-> tail
*/
template <typename T>
class List
{
public:
	struct Node
	{
		T _data;
		Node* _next;
		Node* _prev;
	};

	class iterator
	{
		friend iterator List<T>::erase(iterator iter);

	public:
		iterator(Node *node = nullptr)
		{
			_node = node;
		}

		iterator(const iterator& iter)
		{
			_node = iter._node;
		}

		iterator& operator =(const iterator& iter)
		{
			_node = iter._node;

			return *this;
		}

		iterator operator ++(int)
		{
			//*********************************************************
			// 현재 노드를 ++하기 전에 저장하고 ++한다.
			// return은 저장해둔 노드
			//*********************************************************
			iterator iter = *this;
			operator++();

			return iter;
		}

		iterator& operator ++()
		{
			_node = _node->_next;

			return *this;
		}

		iterator operator--(int)
		{
			iterator iter = *this;
			operator--();

			return iter;
		}

		iterator& operator--()
		{
			_node = _node->_prev;

			return *this;
		}

		iterator* operator ->()
		{
			return this;
		}

		//*****************************************
		// count만큼 next
		//*****************************************
		iterator operator +(int count)
		{
			int num = 0;
			iterator iter = *this;
			while (num < count)
			{
				iter->_node = iter->_node->_next;
				num++;
			}

			return iter;
		}

		//*****************************************
		// count만큼 prev
		//*****************************************
		iterator operator -(int count)
		{
			int num = 0;
			iterator iter = *this;
			while (num < count)
			{
				iter->_node = iter->_node->_prev;
				num++;
			}

			return iter;
		}

		bool operator !=(iterator& iter)
		{
			return !(iter->_node == _node);
		}

		bool operator ==(iterator& iter)
		{
			return (iter->_node == _node);
		}

		//*****************************************
		//현재 노드의 데이터를 뽑음
		//*****************************************
		T& operator *()
		{
			return _node->_data;
		}

	private:
		Node* _node;
	};

	List()
	{
		Node* node = new Node;
		_head = node;

		node = new Node;
		_tail = node;

		_head->_prev = nullptr;
		_head->_next = _tail;
		_tail->_prev = _head;
		_tail->_next = nullptr;
	}

	~List()
	{
		Node* next = _head;
		Node* prev;

		while (next != _tail)
		{
			prev = next;
			next = next->_next;
			delete prev;
		}
		delete next;
	}

	iterator begin()
	{
		return iterator(_head->_next);
	}

	iterator end()
	{
		return iterator(_tail);
	}

	void push_front(T data)
	{
		Node* node = new Node;

		node->_data = data;
		node->_prev = _head;
		node->_next = _head->_next;
		_head->_next->_prev = node;
		_head->_next = node;

		_size++;
	}

	void push_back(T data)
	{
		Node* node = new Node;

		node->_data = data;
		node->_next = _tail;
		node->_prev = _tail->_prev;
		_tail->_prev->_next = node;
		_tail->_prev = node;

		_size++;
	}

	//*********************************************************
	// clear는 파괴자와 다르다. 다시 push를 할 수 있음. 
	// 따라서 head -> tail 관계를 유지 해야함.
	//*********************************************************
	void clear()
	{
		Node* next = _head->_next;
		Node* prev;

		while (next != _tail)
		{
			prev = next;
			next = next->_next;
			delete prev;
			_size--;
		}

		_head->_next = _tail;
		_tail->_prev = _head;
	}

	int size()
	{
		return _size;
	}

	bool is_empty()
	{
		return (_size == 0);
	}

	iterator erase(iterator iter)
	{
		//*********************************************************
		// 잘못된 iterator가 들어 왔을 때는 어쩔 수 없나?
		//*********************************************************

		//*********************************************************
		// node1 node2 node3 상태에서 node2를 지운다면, 
		// 1. node1의 next는 node3이 되어야함.
		// 2. node3의 prev는 node1이 되어야함. 
		//*********************************************************
		iterator prev = iter - 1;
		iterator next = iter + 1;
		prev->_node->_next = next->_node;
		next->_node->_prev = prev->_node;

		//*********************************************************
		// iter의 node를 지우고 다음 노드를 반환하기 위하여,
		//*********************************************************
		iterator save = iter++;
		delete save->_node;
		_size--;

		return iter;
	}

private:


	int _size = 0;
	Node* _head;
	Node* _tail;
};