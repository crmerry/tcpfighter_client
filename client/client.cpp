#include "stdafx.h"
#include "client.h"
#include "AllOfDefine.h"
#include "ScreenDib.h"
#include "SpriteDib.h"
#include "IBaseObject.h"
#include "Character.h"
#include "ClientStruct.h"
#include "my_list.h"
#include "Map.h"
#include "State.h"
#include "PacketDefine.h"
#include "RingBuffer.h"
#include "SerializationBuffer.h"
#include "PacketProcedure.h"
#include "Profiler.h"
#include "Log.h"
/* ***************************************
	필요한 라이브러리
**************************************** */
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "imm32.lib")

/* ***************************************
	초기화 & 정리함수
**************************************** */
bool InitializeEnvrionment();
bool InitializeNetwork();
void InitializeGame();
void CleanUp();

/* ***************************************
	클라이언트 처리 함수.
**************************************** */
bool AllocatePlayer(int* const client_index, int ID, int direction, int x, int y, int HP);
void ReleasePlayer(Character* const player, int ID);
bool FindPlayerByID(Character** const player, int ID);

/* ***************************************
	게임 루프
**************************************** */
void UpdateGame();
void KeyProcess();
void Action();
void SortObject();
void Draw();
bool FrameSkip();

/* ***************************************
	윈도우, 다이얼로그 프로시저.
**************************************** */
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK DialogProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

/* ***************************************
	전역 변수
**************************************** */
ScreenDib g_screen(640, 480, 32);
SpriteDib g_sprite(65, RGB(255, 255, 255));
List<IBaseObject*> g_object_list;
Map* g_map;

SOCKET g_client_socket;
RingBuffer g_send_Q(2048);
RingBuffer g_recv_Q(2048);
Character* g_player;	// KeyProcess를 받을 수 있는 클라이언트
Client* g_client_arr;
int g_player_x;
int g_player_y;

wchar_t g_server_IP[16];
bool g_b_connect = false; // connect 호출시 true
bool g_b_send = false; // WSAEWOULDBLOCK이 뜨면 fasle,
HINSTANCE hInst;
HWND g_wnd;
HIMC hIMC;
bool g_is_active_app = false; 

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE HPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_ADDR), NULL, DialogProc);
	
	UNREFERENCED_PARAMETER(HPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	/* ***************************************
		윈도우 생성
	**************************************** */
	{
		WNDCLASSEXW wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CLIENT));
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_CLIENT);
		wcex.lpszClassName = L"Main";
		wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		RegisterClassExW(&wcex);
	}

	hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

	HWND hWnd = CreateWindowW(L"Main", L"Title", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}
	g_wnd = hWnd;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	RECT window_rect;
	window_rect.top = 0;
	window_rect.left = 0;
	window_rect.right = 640;
	window_rect.bottom = 480;

	AdjustWindowRectEx(&window_rect, GetWindowStyle(g_wnd), GetMenu(g_wnd) != NULL, GetWindowExStyle(g_wnd));
	int center_x = GetSystemMetrics(SM_CXSCREEN) / 2 - 640 / 2;
	int center_y = GetSystemMetrics(SM_CYSCREEN) / 2 - 480 / 2;

	MoveWindow(g_wnd, center_x, center_y, window_rect.right - window_rect.left, window_rect.bottom - window_rect.top, TRUE);

	/* ***************************************
		IME 제거
	**************************************** */
	hIMC = ImmAssociateContext(hWnd, NULL);

	MSG msg;

	/* ***************************************
		네트워크 및 게임 초기화
	**************************************** */
	if (!InitializeEnvrionment() || !InitializeNetwork())
	{
		CleanUp();
	}
	InitializeLog();
	InitializeGame();
	
	/* ***************************************
		게임 루프
	**************************************** */
	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if (g_b_connect)
			{
				UpdateGame();
			}
		}
	}

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HIMC imc;

	switch (message)
	{
	case UM_SOCKET:
	{
		if (WSAGETSELECTERROR(lParam))
		{
			int error = WSAGETSELECTERROR(lParam);
			CleanUp();

			break;
		}

		switch (WSAGETSELECTEVENT(lParam))
		{
		case FD_READ:
			RecvProc();
			break;

		case FD_WRITE:
			g_b_send = true;
			SendProc();
			break;
		
		case FD_CLOSE:
			CleanUp();
			break;

		case FD_CONNECT:
			ConnectProc();
			break;

		default:
			/* ***************************************
				TODO: 잘못된 FD메시지가 올 수가 없을것이고...
				이 부분에선 따로 처리가 필요 없을 듯한데
			**************************************** */
			break;
		}
		break;
	}
	
	case WM_ACTIVATEAPP:
	{
		g_is_active_app = (bool)(wParam);
		break;
	}

	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다.
		EndPaint(hWnd, &ps);
		break;
	}
	
	case WM_DESTROY:
		PROFILE_OUT;
		ImmAssociateContext(hWnd, hIMC);
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK DialogProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HWND edit_box;

	switch (iMsg)
	{
	case WM_INITDIALOG:
		memset(g_server_IP, 0, sizeof g_server_IP);
		edit_box = GetDlgItem(hWnd, IDC_EDIT1);
		SetWindowText(edit_box, L"127.0.0.1");

		return TRUE;

	case WM_COMMAND:
		switch(wParam)
		{
		case IDOK:
			GetDlgItemText(hWnd, IDC_EDIT1, g_server_IP, sizeof g_server_IP / sizeof (wchar_t));
			EndDialog(hWnd, 99939);
			return TRUE;
		
		default:
			return false;
		}
		break;
	default:

		return FALSE;
	}
}

bool InitializeEnvrionment()
{
	timeBeginPeriod(1);
	_wsetlocale(LC_ALL, L"korean");
	PROFILE_INITIALIZE;
	srand(time(NULL));
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		return false;
	}

	
	return true;
}

bool InitializeNetwork()
{
	g_client_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (g_client_socket == INVALID_SOCKET)
	{
		int error = WSAGetLastError();

		return false;
	}

	SOCKADDR_IN server_address;
	memset(&server_address, 0, sizeof server_address);
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(SERVER_PORT);
	InetPton(AF_INET, g_server_IP, &server_address.sin_addr);

	WSAAsyncSelect(g_client_socket, g_wnd, UM_SOCKET, FD_CONNECT | FD_CLOSE | FD_READ | FD_WRITE);

	bool b_nodelay_on_off = 1;

	int ret_val = setsockopt(g_client_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&b_nodelay_on_off, sizeof b_nodelay_on_off);

	if (ret_val == SOCKET_ERROR)
	{
		int error = WSAGetLastError();
		if (error == WSAEWOULDBLOCK)
		{
			return true;
		}

		return false;
	}

	ret_val = connect(g_client_socket, (SOCKADDR*)&server_address, sizeof server_address);

	if (ret_val == SOCKET_ERROR)
	{
		int error = WSAGetLastError();
		if (error == WSAEWOULDBLOCK)
		{
			return true;
		}

		return false;
	}

	return true;
}

void InitializeGame()
{
	/* ***************************************
		스프라이트 로드
	**************************************** */
	{
		//g_sprite.LoadDibSprite(0, SpriteDib::eSpriteName::Map, L".\\SpriteData\\_Map.bmp", 0, 0);
		g_sprite.LoadDibSprite(0, SpriteDib::eSpriteName::Map, L".\\SpriteData\\Tile_01.bmp", 32, 64);

		g_sprite.LoadDibSprite(1, SpriteDib::eSpriteName::Attack1_L_01, L".\\SpriteData\\Attack1_L_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(2, SpriteDib::eSpriteName::Attack1_L_02, L".\\SpriteData\\Attack1_L_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(3, SpriteDib::eSpriteName::Attack1_L_03, L".\\SpriteData\\Attack1_L_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(4, SpriteDib::eSpriteName::Attack1_L_04, L".\\SpriteData\\Attack1_L_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(5, SpriteDib::eSpriteName::Attack1_R_01, L".\\SpriteData\\Attack1_R_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(6, SpriteDib::eSpriteName::Attack1_R_02, L".\\SpriteData\\Attack1_R_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(7, SpriteDib::eSpriteName::Attack1_R_03, L".\\SpriteData\\Attack1_R_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(8, SpriteDib::eSpriteName::Attack1_R_04, L".\\SpriteData\\Attack1_R_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(9, SpriteDib::eSpriteName::Attack2_L_01, L".\\SpriteData\\Attack2_L_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(10, SpriteDib::eSpriteName::Attack2_L_02, L".\\SpriteData\\Attack2_L_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(11, SpriteDib::eSpriteName::Attack2_L_03, L".\\SpriteData\\Attack2_L_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(12, SpriteDib::eSpriteName::Attack2_L_04, L".\\SpriteData\\Attack2_L_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(13, SpriteDib::eSpriteName::Attack2_R_01, L".\\SpriteData\\Attack2_R_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(14, SpriteDib::eSpriteName::Attack2_R_02, L".\\SpriteData\\Attack2_R_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(15, SpriteDib::eSpriteName::Attack2_R_03, L".\\SpriteData\\Attack2_R_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(16, SpriteDib::eSpriteName::Attack2_R_04, L".\\SpriteData\\Attack2_R_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(17, SpriteDib::eSpriteName::Attack3_L_01, L".\\SpriteData\\Attack3_L_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(18, SpriteDib::eSpriteName::Attack3_L_02, L".\\SpriteData\\Attack3_L_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(19, SpriteDib::eSpriteName::Attack3_L_03, L".\\SpriteData\\Attack3_L_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(20, SpriteDib::eSpriteName::Attack3_L_04, L".\\SpriteData\\Attack3_L_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(21, SpriteDib::eSpriteName::Attack3_L_05, L".\\SpriteData\\Attack3_L_05.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(22, SpriteDib::eSpriteName::Attack3_L_06, L".\\SpriteData\\Attack3_L_06.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(23, SpriteDib::eSpriteName::Attack3_R_01, L".\\SpriteData\\Attack3_R_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(24, SpriteDib::eSpriteName::Attack3_R_02, L".\\SpriteData\\Attack3_R_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(25, SpriteDib::eSpriteName::Attack3_R_03, L".\\SpriteData\\Attack3_R_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(26, SpriteDib::eSpriteName::Attack3_R_04, L".\\SpriteData\\Attack3_R_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(27, SpriteDib::eSpriteName::Attack3_R_05, L".\\SpriteData\\Attack3_R_05.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(28, SpriteDib::eSpriteName::Attack3_R_06, L".\\SpriteData\\Attack3_R_06.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(29, SpriteDib::eSpriteName::Move_L_01, L".\\SpriteData\\Move_L_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(30, SpriteDib::eSpriteName::Move_L_02, L".\\SpriteData\\Move_L_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(31, SpriteDib::eSpriteName::Move_L_03, L".\\SpriteData\\Move_L_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(32, SpriteDib::eSpriteName::Move_L_04, L".\\SpriteData\\Move_L_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(33, SpriteDib::eSpriteName::Move_L_05, L".\\SpriteData\\Move_L_05.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(34, SpriteDib::eSpriteName::Move_L_06, L".\\SpriteData\\Move_L_06.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(35, SpriteDib::eSpriteName::Move_L_07, L".\\SpriteData\\Move_L_07.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(36, SpriteDib::eSpriteName::Move_L_08, L".\\SpriteData\\Move_L_08.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(37, SpriteDib::eSpriteName::Move_L_09, L".\\SpriteData\\Move_L_09.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(38, SpriteDib::eSpriteName::Move_L_10, L".\\SpriteData\\Move_L_10.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(39, SpriteDib::eSpriteName::Move_L_11, L".\\SpriteData\\Move_L_11.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(40, SpriteDib::eSpriteName::Move_L_12, L".\\SpriteData\\Move_L_12.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(41, SpriteDib::eSpriteName::Move_R_01, L".\\SpriteData\\Move_R_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(42, SpriteDib::eSpriteName::Move_R_02, L".\\SpriteData\\Move_R_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(43, SpriteDib::eSpriteName::Move_R_03, L".\\SpriteData\\Move_R_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(44, SpriteDib::eSpriteName::Move_R_04, L".\\SpriteData\\Move_R_04.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(45, SpriteDib::eSpriteName::Move_R_05, L".\\SpriteData\\Move_R_05.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(46, SpriteDib::eSpriteName::Move_R_06, L".\\SpriteData\\Move_R_06.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(47, SpriteDib::eSpriteName::Move_R_07, L".\\SpriteData\\Move_R_07.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(48, SpriteDib::eSpriteName::Move_R_08, L".\\SpriteData\\Move_R_08.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(49, SpriteDib::eSpriteName::Move_R_09, L".\\SpriteData\\Move_R_09.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(50, SpriteDib::eSpriteName::Move_R_10, L".\\SpriteData\\Move_R_10.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(51, SpriteDib::eSpriteName::Move_R_11, L".\\SpriteData\\Move_R_11.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(52, SpriteDib::eSpriteName::Move_R_12, L".\\SpriteData\\Move_R_12.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(53, SpriteDib::eSpriteName::Stand_L_01, L".\\SpriteData\\Stand_L_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(54, SpriteDib::eSpriteName::Stand_L_02, L".\\SpriteData\\Stand_L_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(55, SpriteDib::eSpriteName::Stand_L_03, L".\\SpriteData\\Stand_L_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(56, SpriteDib::eSpriteName::Stand_R_01, L".\\SpriteData\\Stand_R_01.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(57, SpriteDib::eSpriteName::Stand_R_02, L".\\SpriteData\\Stand_R_02.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);
		g_sprite.LoadDibSprite(58, SpriteDib::eSpriteName::Stand_R_03, L".\\SpriteData\\Stand_R_03.bmp", CHARACTER_CENTER_X, CHARACTER_CENTER_Y);

		g_sprite.LoadDibSprite(59, SpriteDib::eSpriteName::xSpark_1, L".\\SpriteData\\xSpark_1.bmp", EFFECT_CENTER_X, EFFECT_CENTER_X);
		g_sprite.LoadDibSprite(60, SpriteDib::eSpriteName::xSpark_2, L".\\SpriteData\\xSpark_2.bmp", EFFECT_CENTER_X, EFFECT_CENTER_X);
		g_sprite.LoadDibSprite(61, SpriteDib::eSpriteName::xSpark_3, L".\\SpriteData\\xSpark_3.bmp", EFFECT_CENTER_X, EFFECT_CENTER_X);
		g_sprite.LoadDibSprite(62, SpriteDib::eSpriteName::xSpark_4, L".\\SpriteData\\xSpark_4.bmp", EFFECT_CENTER_X, EFFECT_CENTER_X);

		g_sprite.LoadDibSprite(63, SpriteDib::eSpriteName::Shadow_1, L".\\SpriteData\\Shadow.bmp", SHADOW_CENTER_X, SHADOW_CENTER_Y);

		g_sprite.LoadDibSprite(64, SpriteDib::eSpriteName::HPgauage_1, L".\\SpriteData\\HPGuage.bmp", HPGAUAGE_CENTER_X, HPGAUAGE_CENTER_Y);
	}

	g_sprite.InitializeSpriteToIndexTable();

	g_map = Map::GetInstance(&g_sprite);

	g_client_arr = new Client[CLIENT_COUNT_MAX];
	for (int index = 0; index < CLIENT_COUNT_MAX; index++)
	{
		g_client_arr[index]._ID = CLIENT_NOT_USED;
		g_client_arr[index]._player = NULL;
	}
}

void CleanUp()
{
	WSACleanup();
	timeEndPeriod(1);

	closesocket(g_client_socket);
	SendMessage(g_wnd, WM_DESTROY, 0, 0);
}

bool FindPlayerByID(Character** const player, int ID)
{
	for (int index = 0; index < CLIENT_COUNT_MAX; index++)
	{
		if (g_client_arr[index]._ID == ID)
		{
			*player = g_client_arr[index]._player;

			return true;
		}
	}

	return false;
}

bool AllocatePlayer(int* const client_index, int ID, int direction, int x, int y, int HP)
{
	for (int index = 0; index < CLIENT_COUNT_MAX; index++)
	{
		if (g_client_arr[index]._ID == ID)
		{
			return false;
		}
	}

	for (int index = 0; index < CLIENT_COUNT_MAX; index++)
	{
		if (g_client_arr[index]._ID == CLIENT_NOT_USED || g_client_arr[index]._ID == CLIENT_CLOSED)
		{
			g_client_arr[index]._ID = ID;
			g_client_arr[index]._player = new Character(x, y, IBaseObject::eObjectType::Character, &g_sprite, HP, direction);
			g_object_list.push_back(g_client_arr[index]._player);

			*client_index = index;

			return true;
		}
	}

	return false;
}

void ReleasePlayer(Character* const player, int ID)
{
	List<IBaseObject*>::iterator end = g_object_list.end();
	for (List<IBaseObject*>::iterator iter = g_object_list.begin(); iter != end; ++iter)
	{
		if ((*iter) == player)
		{
			g_object_list.erase(iter);

			break;
		}
	}

	for (int index = 0; index < CLIENT_COUNT_MAX; index++)
	{
		if (g_client_arr[index]._ID == ID)
		{
			g_client_arr[index]._ID = CLIENT_CLOSED;

			break;
		}
	}
}

void UpdateGame()
{
	if (g_is_active_app)
	{
		PROFILE_BEGIN(L"KeyProcess");
		KeyProcess();
		PROFILE_END(L"KeyProcess");
	}
	/////////////////////////////////////////

	//테스트용
	//KeyProcess();
	
	PROFILE_BEGIN(L"Action");
	Action();
	PROFILE_END(L"Action");

	if (!FrameSkip())
	{
		PROFILE_BEGIN(L"Draw");
		Draw();
		PROFILE_END(L"Draw");
	}

	g_screen.DrawBuffer(g_wnd);
}

void KeyProcess()
{
	if (g_player == NULL)
	{
		return;
	}

	short left = GetAsyncKeyState(VK_LEFT) & 0x8000;
	short right = GetAsyncKeyState(VK_RIGHT) & 0x8000;
	short up = GetAsyncKeyState(VK_UP) & 0x8000;
	short down = GetAsyncKeyState(VK_DOWN) & 0x8000;
	short attack1 = GetAsyncKeyState(0x5a) & 0x8000;	//z = 0x5a
	short attack2 = GetAsyncKeyState(0x58) & 0x8000;	//x = 0x58
	short attack3 = GetAsyncKeyState(0x43) & 0x8000;	//c = 0x 43
	short make_npc = GetAsyncKeyState(0x44) & 0x8000;

	int action = 255;
	action = ACTION_STAND;

	//static int choice;
	//
	//static unsigned int start = timeGetTime();
	//unsigned int end = timeGetTime();

	//if (end - start < 1000)
	//{
	//	return;
	//}

	//start = end;

	//choice = rand() % 1000;
	//
	//if (choice >= 0 && choice < 400)
	//{
	//	left = 1;
	//}

	//if (choice >= 0 && choice < 150)
	//{
	//	up = 1;
	//}

	//if (choice >= 150 && choice < 300)
	//{
	//	down = 1;
	//}

	//if (choice >= 400 && choice < 800)
	//{
	//	right = 1;
	//}

	//if (choice >= 400 && choice < 550)
	//{
	//	up = 1;
	//}

	//if (choice >= 550 && choice < 700)
	//{
	//	down = 1;
	//}

	//if (choice >= 800 && choice < 860)
	//{
	//	attack1 = 1;
	//}

	//if (choice >= 860 && choice < 920)
	//{
	//	attack2 = 1;
	//}
	//if (choice >= 920 && choice < 1000)
	//{
	//	attack3 = 1;
	//}

	if (left && up)
	{
		action = ACTION_MOVE_LU;
	}
	else if (right && up)
	{
		action = ACTION_MOVE_RU;
	}
	else if (right && down)
	{
		action = ACTION_MOVE_RD;
	}
	else if (left && down)
	{
		action = ACTION_MOVE_LD;
	}
	else if (left)
	{
		action = ACTION_MOVE_LL;
	}
	else if (up)
	{
		action = ACTION_MOVE_UU;
	}
	else if (right)
	{
		action = ACTION_MOVE_RR;
	}
	else if (down)
	{
		action = ACTION_MOVE_DD;
	}
	else
	{

	}

	if (attack1)
	{
		action = ACTION_ATTACK1;
	}
	else if (attack2)
	{
		action = ACTION_ATTACK2;
	}
	else if (attack3)
	{
		action = ACTION_ATTACK3;
	}
	else if (make_npc)
	{

	}
	else
	{

	}

	g_player->InputCharacterAction(action);
}

void Action()
{
	List<IBaseObject*>::iterator iter = g_object_list.begin();
	List<IBaseObject*>::iterator end = g_object_list.end();

	while (iter != end)
	{
		if (!(*iter)->Action())
		{
			IBaseObject* deleted_object = *iter;
			g_object_list.erase(iter++);
			delete deleted_object;
		}
		else
		{
			++iter;
		}
	}

	SortObject();
}

void SortObject()
{
	/* ***************************************
		이펙트 -> 더 큰 Y값을 가진 오브젝트 순으로 정렬
	**************************************** */
	int size = g_object_list.size();

	IBaseObject** stack = (IBaseObject**)malloc(sizeof(IBaseObject*)*size);

	List<IBaseObject*>::iterator iter;
	List<IBaseObject*>::iterator max_iter;
	List<IBaseObject*>::iterator end;

	int stack_index = 0;
	iter = g_object_list.begin();
	end = g_object_list.end();

	/* ***************************************
		전역 오브젝트 리스트에서 이펙트만 빼서
		스택 배열에 넣는다.
	**************************************** */
	while (iter != end)
	{
		if ((*iter)->GetType() == IBaseObject::eObjectType::Effect)
		{
			stack[stack_index++] = *iter;
			g_object_list.erase(iter++);
		}
		else
		{
			++iter;
		}
	}

	/* ***************************************
		스택 배열에 있는 이펙트를 Y가 큰 기준으로
		정렬.
	**************************************** */
	for (int index = 0; index < stack_index; index++)
	{
		for (int next_index = index; next_index < stack_index; next_index++)
		{
			if (stack[index]->GetY() <= stack[next_index]->GetY())
			{
				IBaseObject* temp = stack[index];
				stack[index] = stack[next_index];
				stack[next_index] = temp;
			}
		}
	}

	/* ***************************************
		전역 오브젝트 배열에서 큰 y부터 스택
		배열에 삽입. 하고 삭제.
	**************************************** */
	for (int index = stack_index; index < size; index++)
	{
		max_iter = g_object_list.begin();

		for (iter = g_object_list.begin(); iter != g_object_list.end(); ++iter)
		{
			if ((*max_iter)->GetY() <= (*iter)->GetY())
			{
				max_iter = iter;
			}
		}

		stack[index] = *max_iter;
		g_object_list.erase(max_iter);
	}

	/* ***************************************
		스택 배열의 처음부터 전역 오브젝트에 삽입.
	**************************************** */
	for (int index = 0; index < size; index++)
	{
		g_object_list.push_front(stack[index]);
	}
}

void Draw()
{
	if (g_player)
	{
		g_map->DrawMap(g_player->GetX(), g_player->GetY(), g_screen.GetDibBuffer(), g_screen.GetWidth(), g_screen.GetHeight(), g_screen.GetPtich());
	}
	
	List<IBaseObject*>::iterator iter;
	for (iter = g_object_list.begin(); iter != g_object_list.end(); ++iter)
	{
		(*iter)->Draw();
	}
}

bool FrameSkip()
{
	// 윈도우 타이틀 바에 FPS 출력
	{
		static unsigned long FPS_check_start = timeGetTime();
		unsigned long FPS_check_end = timeGetTime();
		static int count = 1;

		if (FPS_check_end - FPS_check_start >= 1000)
		{
			wchar_t temp[10];
			swprintf_s(temp, 10, L"%d", count);
			SetWindowText(g_wnd, temp);

			FPS_check_start = timeGetTime();
			count = 1;
		}

		count++;
	}
	
	// 이번 프레임을 스킵할지 그릴지 결정.
	{
		static unsigned long start = timeGetTime();
		unsigned long end = timeGetTime();
		static int accumulated_seconds = 0;
		const int SPF = 20;

		if (end - start >= SPF)
		{
			accumulated_seconds += end - start;

			if (accumulated_seconds >= SPF)
			{
				accumulated_seconds -= SPF;
				start = timeGetTime();

				return true;
			}
			else
			{
				start = timeGetTime();

				return false;
			}
		}
		else
		{
			int sleep_amount = SPF - (end - start);
			Sleep(sleep_amount);
			start = timeGetTime();

			return false;
		}
	}

	return true;
}