#pragma once

/* ***************************************
	FD 메시지
**************************************** */
bool SendProc();
void RecvProc();
void ConnectProc();
void CloseProc();

/* ***************************************
	패킷 생성
**************************************** */
void CreatePacketHeader(SerializationBuffer* const packet, unsigned char code, unsigned char size, unsigned char type);
void CreatePacketMoveStartCS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketMoveStopCS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketAttack1CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketAttack2CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketAttack3CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);

/* ***************************************
	송신부 : 컨텐츠쪽 멤버함수 참고.
**************************************** */
bool SendPacket(SerializationBuffer* const packet);
// FD메시지의 SendProc도 해당

/* ***************************************
	수신부
**************************************** */
// FD메시지의 RecvProc도 해당
void RecvPacket();
bool DecodePacket(unsigned char* type, SerializationBuffer* const packet);
void ProcPacket(unsigned char type, SerializationBuffer* const packet);

void MoveStartSC(SerializationBuffer* const packet);
void MoveStopSC(SerializationBuffer* const packet);
void DamageSC(SerializationBuffer* const packet);
void CreateMyCharacter(SerializationBuffer* const packet);
void CreateOtherCharacter(SerializationBuffer* const packet);
void DeleteCharacter(SerializationBuffer* const packet);
void Attack1SC(SerializationBuffer* const packet);
void Attack2SC(SerializationBuffer* const packet);
void Attack3SC(SerializationBuffer* const packet);
void PackySyncSC(SerializationBuffer* const packet);