#include "stdafx.h"
#include "AllOfDefine.h"
#include "Map.h"
#include "SpriteDib.h"
#include "IBaseObject.h"
#include "Character.h"

extern Character* g_player;
extern HWND g_wnd;

Map::Map(SpriteDib* sprite_dib, int map_index)
{
	_sprite_dib = sprite_dib;
	_map_index = map_index;

	/* ***************************************
	tile_map에 타일 인덱스를 설정.
	**************************************** */
	char tile_map[20][20] =
	{
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};

	memcpy_s(_tile_map, sizeof(char) * 20 * 20, tile_map, sizeof(char) * 20 * 20);
}

Map::~Map()
{
}

Map* Map::GetInstance(SpriteDib* sprite_dib)
{
	int map_index = sprite_dib->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Map);
	static Map map(sprite_dib, map_index);
	
	return &map;
}

void Map::DrawMap(int character_x, int character_y, BYTE* dest, int width, int height, int pitch)
{
	int world_left = character_x - 320;
	int world_top = character_y - 240;
	
	/* ***************************************
	사이드인 경우 후 처리 필요.
	**************************************** */
	world_left = min(5760, world_left);
	world_left = max(0, world_left);
	
	world_top = min(5920, world_top);
	world_top = max(0, world_top);

	/* ***************************************
	중점을 기준으로 출력을 시행하므로
	중점에 맞추어줌.
	**************************************** */
	int draw_x = TILE_CENTER_X;
	int draw_y = TILE_SIZE;

	int tile_x;
	int tile_y;

	int tile_move_x;
	int tile_move_y;

	int tile_index;

	for (int y = 0; y < 22; y++)
	{
		for (int x = 0; x < 21; x++)
		{
			/* ***************************************
			타일 위치 및 타일 인덱스를 계산.
			타일에서 어느 지점만큼 떨어져서 출력할 것인지.
			**************************************** */
			tile_x = world_left / TILE_SIZE;
			tile_y = world_top / TILE_SIZE;

			tile_move_x = world_left % TILE_SIZE;
			tile_move_y = world_top % TILE_SIZE;

			tile_index = _tile_map[tile_y][tile_x];

			_sprite_dib->DrawSprite(0, draw_x - tile_move_x, draw_y - tile_move_y, dest, width, height, pitch);

			draw_x += TILE_SIZE;
			tile_x += 1;
		}

		tile_y += 1;
		draw_x = TILE_CENTER_X;
		draw_y += TILE_SIZE;
	}
}

void Map::ChangeMap(int map_index)
{
	_map_index = map_index;
}