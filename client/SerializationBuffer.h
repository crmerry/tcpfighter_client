#ifndef  __SERIALIZATION_BUFFER__
#define  __SERIALIZATION_BUFFER__

class SerializationBuffer
{
public:
	enum eSerializationBuffer
	{
		BUFFER_DEFAULT = 2048
	};
	SerializationBuffer(int size = eSerializationBuffer::BUFFER_DEFAULT);
	virtual	~SerializationBuffer();

	/* ***************************************
	TODO:кл┼Х к─▒Ф?
	**************************************** */
	void	Release();
	void	Clear();
	int		GetBufferSize() const;
	int		GetDataSize() const;
	char*	GetBufferPtr() const;
	int		MoveWritePos(int size);
	int		MoveReadPos(int size);
	int		GetData(char* dest, int size);
	int		PutData(char* src, int size);

	SerializationBuffer& operator =	 (const SerializationBuffer& copy);
	SerializationBuffer& operator << (BYTE		value);
	SerializationBuffer& operator << (char		value);
	SerializationBuffer& operator << (short		value);
	SerializationBuffer& operator << (WORD		value);
	SerializationBuffer& operator << (int		value);
	SerializationBuffer& operator << (DWORD		value);
	SerializationBuffer& operator << (float		value);
	SerializationBuffer& operator << (__int64	value);
	SerializationBuffer& operator << (double	value);
	SerializationBuffer& operator << (wchar_t*	value);

	SerializationBuffer& operator >> (BYTE&		value);
	SerializationBuffer& operator >> (char&		value);
	SerializationBuffer& operator >> (short&	value);
	SerializationBuffer& operator >> (WORD&		value);
	SerializationBuffer& operator >> (int&		value);
	SerializationBuffer& operator >> (DWORD&	value);
	SerializationBuffer& operator >> (float&	value);
	SerializationBuffer& operator >> (__int64&	value);
	SerializationBuffer& operator >> (double&	value);
	SerializationBuffer& operator >> (wchar_t*	value);
protected:
	char* _buffer;
	int _size;
	int _front;
	int _rear;
};

#endif