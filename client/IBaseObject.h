#pragma once

class SpriteDib;
class State;

class IBaseObject
{
public:
	enum class eObjectType
	{
		Character,
		NPC,
		Effect,
		Enemy,
	};

	IBaseObject(int x, int y, eObjectType type, SpriteDib* const sprite_dib);
	~IBaseObject();

	virtual void Draw() = 0;
	virtual bool Action() = 0; 

	int GetX() const;
	int GetY() const;
	int GetSpriteIndex() const;
	int GetSpriteStart() const;
	int GetSpriteEnd() const;
	int GetDelayCount() const;
	int GetDelayPerFrame() const;
	eObjectType GetType() const;
	bool IsEndSprite() const;

	void SetX(int x);
	void SetY(int y);
	void SetSpriteIndex(int index);
	void SetSpriteStart(int sprite_start);
	void SetSpriteEnd(int sprite_end);
	void SetDelayPerFrame(int delay_max);
	void SetDelayCount(int delay_count);
	void ChangeSprite(int index, int start, int end, int delay_per_frame, int delay_count);
	void ReturnStartSprite();
	bool ChangeCurrentFrameToNextFrame();
	SpriteDib* GetSpriteDib() const;
	
private:
	int _x;
	int _y;
	int _sprite_index;
	int _sprite_start;
	int _sprite_end;
	int _delay_per_frame;
	int _delay_count;
	bool _is_end_sprite;
	eObjectType _type;
	SpriteDib* _sprite_dib;
};