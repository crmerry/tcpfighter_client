#pragma once

class SpriteDib;

class Effect : public IBaseObject
{
public:
	Effect(int x, int y, eObjectType type, SpriteDib* const sprite_dib);
	bool Action() override;
	void Draw() override;
private:
};