#include "stdafx.h"
#include "AllOfDefine.h"
#include "my_list.h"
#include "RingBuffer.h"
#include "IBaseObject.h"
#include "Character.h"
#include "ClientStruct.h"
#include "PacketDefine.h"
#include "SerializationBuffer.h"
#include "PacketProcedure.h"
#include "Log.h"
/* ***************************************
	외부 함수 : client 처리
**************************************** */
extern void CleanUp();
extern bool AllocatePlayer(int* const client_index, int ID, int direction, int x, int y, int HP);
extern void ReleasePlayer(Character* const player, int ID);
extern bool FindPlayerByID(Character** const player, int ID);

/* ***************************************
	외부 전역 변수
**************************************** */
extern SOCKET g_client_socket;
extern RingBuffer g_send_Q;
extern RingBuffer g_recv_Q;
extern Client* g_client_arr;
extern SpriteDib g_sprite;
extern List<IBaseObject*> g_object_list;
extern Character* g_player;
extern bool g_b_connect;
extern bool g_b_send;

/* ***************************************
	FD 메시지
**************************************** */
void ConnectProc()
{
	g_b_connect = true;
}

void RecvProc()
{
	RecvPacket();

	while (1)
	{
		SerializationBuffer packet;
		unsigned char type;

		if (!DecodePacket(&type, &packet))
		{
			break;
		}

		ProcPacket(type, &packet);
	}
}

void CloseProc()
{
	CleanUp();
}

bool SendProc()
{
	if (!g_b_send)
	{
		return false;
	}

	while (1)
	{
		if ((g_send_Q.GetUsedSize()) == 0)
		{
			return false;
		}

		int send_size = send(g_client_socket, g_send_Q.GetReadBufferPtr(), g_send_Q.GetReadableSizeAtOnce(), 0);
		
		if (send_size == SOCKET_ERROR)
		{
			int error = WSAGetLastError();

			if (error == WSAEWOULDBLOCK)
			{
				g_b_send = false;

				return true;
			}

			/* ***************************************
				접속 끊고 로그
			**************************************** */
			CleanUp();
			
			return false;
		}

		g_send_Q.MoveFrontAfterRead(send_size);
	}
}

/* ***************************************
	패킷 생성
**************************************** */
void CreatePacketHeader(SerializationBuffer* const packet, unsigned char code, unsigned char size, unsigned char type)
{
	*packet << code;
	*packet << size;
	*packet << type;
	*packet << (unsigned char)0; // temp 쓰지 않느다.
}

void CreatePacketMoveStartCS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	CreatePacketHeader(packet, PACKET_HEADER_CODE, 5, PACKET_MOVE_START_CS);
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketMoveStopCS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	CreatePacketHeader(packet, PACKET_HEADER_CODE, 5, PACKET_MOVE_STOP_CS);
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketAttack1CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	CreatePacketHeader(packet, PACKET_HEADER_CODE, 5, PACKET_ATTACK1_CS);
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketAttack2CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	CreatePacketHeader(packet, PACKET_HEADER_CODE, 5, PACKET_ATTACK2_CS);
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketAttack3CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	CreatePacketHeader(packet, PACKET_HEADER_CODE, 5, PACKET_ATTACK3_CS);
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

/* ***************************************
	송신부 
**************************************** */
bool SendPacket(SerializationBuffer* const packet)
{
	int data_size = packet->GetDataSize();

	int enqueue_size = g_send_Q.Enqueue(packet->GetBufferPtr(), data_size);

	if (enqueue_size != data_size)
	{
		return false;
	}

	return true;
}

/* ***************************************
	수신부
**************************************** */
void RecvPacket()
{
	int read_count = recv(g_client_socket, g_recv_Q.GetWriteBufferPtr(), g_recv_Q.GetWritableSizeAtOnce(), 0);
	
	if (read_count > 0)
	{
		g_recv_Q.MoveRearAfterWrite(read_count);
	}
	else if (read_count == 0)
	{
		/* ***************************************
			FIN 이나 RST가 온것.
			즉 상대방에서 더 이상 보낼 것이 없다는 표시에
			의하여, 내 소켓도 recv에 0을 반환.
		**************************************** */
		CleanUp();
	}
	else if (read_count == SOCKET_ERROR)
	{
		int error = WSAGetLastError();

		if (error == WSAEWOULDBLOCK)
		{
			return;
		}

		/* ***************************************
			접속 끊고 로그
		**************************************** */
		CleanUp();
	}
}

bool DecodePacket(unsigned char* type, SerializationBuffer* const packet)
{
	PacketHeader header;

	if (g_recv_Q.GetUsedSize() < sizeof(PacketHeader))
	{
		return false;
	}

	g_recv_Q.Peek((char*)&header, sizeof(PacketHeader));
	*type = header._type;

	if (header._code != PACKET_HEADER_CODE)
	{
		CleanUp();

		return false;
	}

	int total_packet_size = sizeof(PacketHeader) + header._size + PACKET_END_CODE_SIZE;
	
	if (g_recv_Q.GetUsedSize() < total_packet_size)
	{
		return false;
	}

	g_recv_Q.MoveFrontAfterRead(sizeof(PacketHeader));

	int payload_size = header._size;
	g_recv_Q.Dequeue(packet->GetBufferPtr(), payload_size);
	packet->MoveWritePos(payload_size);
	
	char end_code;
	g_recv_Q.Dequeue(&end_code, sizeof end_code);
	
	if (end_code != PACKET_END_CODE)
	{
		CleanUp();

		return false;
	}

	return true;
}

void ProcPacket(unsigned char type, SerializationBuffer* const packet)
{
	switch (type)
	{
	case PACKET_MOVE_START_SC:
		MoveStartSC(packet);
		break;

	case PACKET_MOVE_STOP_SC:
		MoveStopSC(packet);
		break;

	case PACKET_DAMAGE_SC:
		DamageSC(packet);
		break;

	case PACKET_CREATE_MY_CHARACTER_SC:
		CreateMyCharacter(packet);
		break;

	case PACKET_CREATE_OTHER_CHARACTER_SC:
		CreateOtherCharacter(packet);
		break;

	case PACKET_DELETE_CHARACTER_SC:
		DeleteCharacter(packet);
		break;

	case PACKET_ATTACK1_SC:
		Attack1SC(packet);
		break;

	case PACKET_ATTACK2_SC:
		Attack2SC(packet);
		break;

	case PACKET_ATTACK3_SC:
		Attack3SC(packet);
		break;

	case PACKET_SC_SYNC:
		PackySyncSC(packet);
		break;

	default:
		/* ***************************************
			서버에서 패킷이 잘못 온 것 
			or
			클라에서 패킷을 잘못 해석한것.
			접속종료처리 + 로그 처리
		**************************************** */
		CleanUp();
		break;
	}
}

void MoveStartSC(SerializationBuffer* const packet)
{
	int ID;
	unsigned char direction;
	unsigned short x;
	unsigned short y;

	*packet >> ID;
	*packet >> direction;
	*packet >> x;
	*packet >> y;

	Character* target;

	if (!FindPlayerByID(&target, ID))
	{
		/* ***************************************
		TODO:예외처리
		해당 섹터에 아이디가 없다?
		서버 : 해당 섹터에선 이 ID가 있으므로, 보내줌.
		클라 : 내 섹터엔 이 ID가 없는데?
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"MoveStartSC. my_pos : (%d, %d), packet_pos : (%d, %d)", g_player->GetX(), g_player->GetY(), x, y);

		return;
	}

	if (target == g_player)
	{
		return;
	}

	target->InputCharacterAction(direction);
	target->SetX(x);
	target->SetY(y);
}

void MoveStopSC(SerializationBuffer* const packet)
{
	int ID;
	unsigned char direction;
	unsigned short x;
	unsigned short y;

	*packet >> ID;
	*packet >> direction;
	*packet >> x;
	*packet >> y;

	Character* target;

	if (!FindPlayerByID(&target, ID))
	{
		/* ***************************************
		TODO:예외처리
		해당 섹터에 아이디가 없다?
		서버 : 해당 섹터에선 이 ID가 있으므로, 보내줌.
		클라 : 내 섹터엔 이 ID가 없는데?
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"MoveStopSC. my_pos : (%d, %d), packet_pos : (%d, %d)", g_player->GetX(), g_player->GetY(), x, y);

		return;
	}

	if (target == g_player)
	{
		return;
	}

	target->InputCharacterAction(ACTION_STAND);
	target->SetX(x);
	target->SetY(y);
}

void DamageSC(SerializationBuffer* const packet)
{
	int	attack_ID;
	int	damage_ID;
	unsigned char HP;

	*packet >> attack_ID;
	*packet >> damage_ID;
	*packet >> HP;

	Character* target;

	if (!FindPlayerByID(&target, damage_ID))
	{
		/* ***************************************
		TODO:예외처리
		해당 섹터에 아이디가 없다?
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"DamageSC. my_pos : (%d, %d), attack_ID: %d, damage_ID : %d", g_player->GetX(), g_player->GetY(), attack_ID, damage_ID);

		return;
	}

	Character* attacker;

	if (FindPlayerByID(&attacker, attack_ID))
	{
		/* ***************************************
		서버에서 공격자의 섹터와 피공격자의 섹터가 다를 수 있다.
		이 때, 해당 클라가 이를 관전하고 있는 상태인데 또 공격자가 보이지않으며
		피공격자만 보일 때, 공격자에 대한 정보가 없을 수 있다.
		**************************************** */
		attacker->SetEffect(true);
	}

	target->SetHP(HP);
	
}

void CreateMyCharacter(SerializationBuffer* const packet)
{
	int	ID;
	unsigned char direction;
	unsigned short x;
	unsigned short y;
	unsigned char HP;

	*packet >> ID;
	*packet >> direction;
	*packet >> x;
	*packet >> y;
	*packet >> HP;

	int index;

	if (!AllocatePlayer(&index, ID, direction, x, y, HP))
	{		
		/* ***************************************
		TODO:예외처리
		생성 실패는.. 아이디가 같다든지, 생성 수를 초과하는경우
		**************************************** */
		CleanUp();

		return;
	}

	g_player = g_client_arr[index]._player;
}

void CreateOtherCharacter(SerializationBuffer* const packet)
{
	int	ID;
	unsigned char direction;
	unsigned short x;
	unsigned short y;
	unsigned char HP;

	*packet >> ID;
	*packet >> direction;
	*packet >> x;
	*packet >> y;
	*packet >> HP;

	int index;

	if (!AllocatePlayer(&index, ID, direction, x, y, HP))
	{
		/* ***************************************
		TODO:예외처리
		생성 실패는.. 아이디가 같다든지, 생성 수를 초과하는경우
		**************************************** */
		CleanUp();

		return;
	}
}

void DeleteCharacter(SerializationBuffer* const packet)
{
	int ID;

	*packet >> ID;

	Character* target;

	if (!FindPlayerByID(&target, ID))
	{
		/* ***************************************
		TODO:예외처리
		캐릭을 삭제하라 보내줬는데, 플레이어 배열에 캐릭이 없네?
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"DeleteCharacter. my_pos : (%d, %d), target_ID : %d", g_player->GetX(), g_player->GetY(), ID);

		return;
	}

	ReleasePlayer(target, ID);
}

void Attack1SC(SerializationBuffer* const packet)
{
	int ID;
	unsigned char direction;
	unsigned short x;
	unsigned short y;

	*packet >> ID;
	*packet >> direction;
	*packet >> x;
	*packet >> y;

	Character* target;

	if (!FindPlayerByID(&target, ID))
	{
		/* ***************************************
		TODO:예외처리
		서버에선 ID에 해당하는 캐릭터가 나를 때린 것인데...
		ID에 해당하는 캐릭이 없다. 끊어줘야하나?
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Attack1SC. my_pos : (%d, %d), packet_ID : %d, packet_pos : (%d, %d)", g_player->GetX(), g_player->GetY(), ID, x, y);
		return;
	}

	if (target == g_player)
	{
		return;
	}

	target->SetFixedAction(false);
	target->InputCharacterAction(ACTION_ATTACK1);
	target->SetDirection(direction);
}

void Attack2SC(SerializationBuffer* const packet)
{
	int ID;
	unsigned char direction;
	unsigned short x;
	unsigned short y;

	*packet >> ID;
	*packet >> direction;
	*packet >> x;
	*packet >> y;

	Character* target;

	if (!FindPlayerByID(&target,ID))
	{
		/* ***************************************
		TODO:예외처리
		서버에선 ID에 해당하는 캐릭터가 나를 때린 것인데...
		ID에 해당하는 캐릭이 없다. 끊어줘야하나?
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Attack1SC. my_pos : (%d, %d), packet_ID : %d, packet_pos : (%d, %d)", g_player->GetX(), g_player->GetY(), ID, x, y);

		return;
	}

	if (target == g_player)
	{
		return;
	}

	target->SetFixedAction(false);
	target->InputCharacterAction(ACTION_ATTACK2);
	target->SetDirection(direction);
}

void Attack3SC(SerializationBuffer* const packet)
{
	int ID;
	unsigned char direction;
	unsigned short x;
	unsigned short y;

	*packet >> ID;
	*packet >> direction;
	*packet >> x;
	*packet >> y;

	Character* target;

	if (!FindPlayerByID(&target, ID))
	{
		/* ***************************************
		TODO:예외처리
		서버에선 ID에 해당하는 캐릭터가 나를 때린 것인데...
		ID에 해당하는 캐릭이 없다. 끊어줘야하나?
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Attack1SC. my_pos : (%d, %d), packet_ID : %d, packet_pos : (%d, %d)", g_player->GetX(), g_player->GetY(), ID, x, y);

		return;
	}

	if (target == g_player)
	{
		return;
	}

	target->SetFixedAction(false);
	target->InputCharacterAction(ACTION_ATTACK3);
	target->SetDirection(direction);
}

void PackySyncSC(SerializationBuffer* const packet)
{
	int ID;
	unsigned short x;
	unsigned short y;

	*packet >> ID;
	*packet >> x;
	*packet >> y;

	Character* target;

	if (!FindPlayerByID(&target, ID))
	{
		/* ***************************************
		TODO:예외처리
		이 패킷은 후에 섹터 단위로 유저들의 위치를 
		보정해주기 위한 것.
		그런데, 서버에서는 이 섹터에 유저가 있다고 판단해서 보냈는데,
		클라에선 이 섹터에 유저가 없다... 그렇다면 어찌 해야하나?
		**************************************** */
		//CleanUp();

		return;
	}

	target->SetX(x);
	target->SetY(y);
}