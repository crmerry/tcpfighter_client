#pragma once

class SpriteDib;

class Character : public IBaseObject
{
public:
	enum class eDirection
	{
		Left = ACTION_MOVE_LL, Right = ACTION_MOVE_RR
	};
	Character(int x, int y, eObjectType type, SpriteDib* sprite_dib, int hp, int direction);
	~Character();
	virtual void Draw() override;
	virtual bool Action() override;
	void InputCharacterAction(int action);
	void SetCharacterActionAttackSprite();
	void SetCharacterActionMoveStandSprite();
	void ChangeIntoCharacterActionMoveSprite();
	void ChangeIntoCharacterActionStandSprite();
	void ChangeIntoCharacterActionAttack1Sprite();
	void ChangeIntoCharacterActionAttack2Sprite();
	void ChangeIntoCharacterActionAttack3Sprite();
	void NextCharacterFrame();
	void SetDirection(unsigned char direction);
	void SetHP(int HP);
	void SetFixedAction(bool b_fixed);
	void SetEffect(bool b_make);
	void CreateEffect();

	/* ***************************************
		匙飘况农 价脚 贸府 : PacketProcedure.cpp
	**************************************** */
	void SendCharacterPacket();
	void SendPacketMoveStopCS();
	void SendPacketMoveStartCS();
	void SendPacketAttack1CS();
	void SendPacketAttack2CS();
	void SendPacketAttack3CS();

private:
	int		_HP;
	bool	_b_live;
	bool	_b_fixed_action;
	bool	_b_sprite_end;
	bool	_b_create_effect;
	int		_action_old;
	int		_action;
	eDirection _direction;
};