#include "stdafx.h"
#include "AllOfDefine.h"
#include "IBaseObject.h"
#include "SpriteDib.h"
#include "ScreenDib.h"
#include "Effect.h"

extern ScreenDib g_screen;
extern int g_player_x;
extern int g_player_y;

Effect::Effect(int x, int y, eObjectType type, SpriteDib * const sprite_dib)
	:IBaseObject(x, y, type, sprite_dib)
{
	int sprite_start = sprite_dib->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::xSpark_1);
	int sprite_index = sprite_start;
	int sprite_end = sprite_dib->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::xSpark_End);

	SetSpriteStart(sprite_start);
	SetSpriteIndex(sprite_index);
	SetSpriteEnd(sprite_end);
	SetDelayCount(0);
	SetDelayPerFrame(DELAY_EFFECT);
}

bool Effect::Action()
{
	return ChangeCurrentFrameToNextFrame();
}

void Effect::Draw()
{
	SpriteDib* sprite_dib = GetSpriteDib();

	const int x = GetX();
	const int y = GetY();

	int world_x = g_player_x - 320;
	int world_y = g_player_y - 240;

	int effect_x = x - world_x;
	int effect_y = y - world_y;

	if (world_x < 0)
	{
		effect_x = x;
	}
	else if (world_x > 5760)
	{
		effect_x = x - 5760;
	}

	if (world_y < 0)
	{
		effect_y = y;
	}
	else if (world_y > 5920)
	{
		effect_y = y - 5920;
	}

	sprite_dib->DrawSprite(GetSpriteIndex(), effect_x, effect_y, g_screen.GetDibBuffer(), g_screen.GetWidth(), g_screen.GetHeight(), g_screen.GetPtich(), false);
}