#pragma once

class ScreenDib
{
public:
			ScreenDib(int width, int height, int color_bit);
	virtual ~ScreenDib();
	void	DrawBuffer(HWND wnd, int x=0, int y=0);
	BYTE*	GetDibBuffer();
	int		GetWidth();
	int		GetHeight();
	int		GetPtich();

protected:
	void	CreateDibBuffer(int width, int height, int color_bit);
	void	ReleaseDibBuffer();

	BITMAPINFO	_dib_info;
	BYTE*		_buffer;
	
	int	_width;
	int	_height;
	int	_pitch; // 4바이트 정렬된 한 줄의 바이트 수
	int	_color_bit;
	int	_buffer_size;
};