class IBaseObject;

class State
{
public:
	virtual void Excute(IBaseObject* npc) = 0;
	virtual void Exit() = 0;
	virtual void Enter(IBaseObject* npc) = 0;

protected:
	State()
	{

	}
	~State()
	{

	}
};

class MoveUpRightState : public State
{
public:
	virtual void Excute(IBaseObject* npc) override;
	virtual void Exit() override;
	virtual void Enter(IBaseObject* npc) override;
	static MoveUpRightState* GetInstance();
private:
	MoveUpRightState()
	{

	}
	~MoveUpRightState()
	{

	}
};

class MoveDownLeftState : public State
{
public:
	virtual void Excute(IBaseObject* npc) override;
	virtual void Exit() override;
	virtual void Enter(IBaseObject* npc) override;
	static MoveDownLeftState* GetInstance();
private:
	MoveDownLeftState()
	{

	}
	~MoveDownLeftState()
	{

	}
};