#include "stdafx.h"
#include "AllOfDefine.h"
#include "IBaseObject.h"
#include "SpriteDib.h"
#include "ScreenDib.h"
#include "Character.h"
#include "ClientStruct.h"
#include "Effect.h"
#include "my_list.h"
#include "RingBuffer.h"
#include "PacketDefine.h"
#include "SerializationBuffer.h"
#include "PacketProcedure.h"
/* ***************************************
	외부 전역 함수
**************************************** */
extern void CleanUp();

/* ***************************************
	extern 전역 변수
**************************************** */
extern ScreenDib g_screen;
extern List<IBaseObject*> g_object_list;
extern HWND g_wnd;

extern Character* g_player;
extern int g_player_x;
extern int g_player_y;

extern bool g_b_send;

Character::Character(int x, int y, eObjectType type, SpriteDib* sprite_dib, int HP, int direction)
	:IBaseObject(x, y, type, sprite_dib)
{
	_HP = HP;
	_b_live = true;
	_b_fixed_action = false;
	_b_sprite_end = false;
	_b_create_effect = false;
	_action_old = ACTION_STAND;
	_action = ACTION_STAND;
	_direction = static_cast<eDirection>(direction);
}

Character::~Character()
{

}

/* ***************************************
그림자 -> HP -> 캐릭터 스프라이트 순으로 그린다.
**************************************** */
void Character::Draw()
{
	SpriteDib* const sprite = GetSpriteDib();

	const int shadow_index = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Shadow_1);
	const int HPgauage_index = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::HPgauage_1);
	const int character_index = GetSpriteIndex();

	const int x = GetX();
	const int y = GetY();

	int world_x = g_player_x - 320;
	int world_y = g_player_y - 240;

	int character_x = x - world_x;
	int character_y = y - world_y;

	if (world_x < 0)
	{
		character_x = x;
	}
	else if (world_x > 5760)
	{
		character_x = x - 5760;
	}

	if (world_y < 0)
	{	
		character_y = y;	
	}
	else if (world_y > 5920)
	{
		character_y = y - 5920;
	}

	//wchar_t temp[20];
	//swprintf_s(temp, 20, L"%d %d %d %d", world_x, world_y, g_player->GetX(), g_player->GetY());
	//SetWindowText(g_wnd, temp);

	ScreenDib* const dest_screen = &g_screen;
	BYTE* const dest_buffer = dest_screen->GetDibBuffer();
	const int dest_width = dest_screen->GetWidth();
	const int dest_height = dest_screen->GetHeight();
	const int dest_pitch = dest_screen->GetPtich();

	if (g_player == this)
	{
		sprite->DrawSprite(shadow_index, character_x, character_y, dest_buffer, dest_width, dest_height, dest_pitch, true);
		sprite->DrawSprite(HPgauage_index, character_x, character_y + 10, dest_buffer, dest_width, dest_height, g_screen.GetPtich(), true, _HP);
		sprite->DrawSprite(character_index, character_x, character_y, dest_buffer, dest_width, dest_height, g_screen.GetPtich(), true);
	}
	else
	{
		sprite->DrawSprite(shadow_index, character_x, character_y, dest_buffer, dest_width, dest_height, dest_pitch, true);
		sprite->DrawSprite(HPgauage_index, character_x, character_y + 10, dest_buffer, dest_width, dest_height, g_screen.GetPtich(), false, _HP);
		sprite->DrawSprite(character_index, character_x, character_y, dest_buffer, dest_width, dest_height, g_screen.GetPtich(), false);
	}
}

bool Character::Action()
{
	//if (_HP < 1)
	//{
	//	_b_live = false;

	//	return _b_live;
	//}

	/* ***************************************
	ACTION_ATTACK123은 다음 입력이 무엇이 됐든
	해당 액션을 유지해야 하고, 끝난 다음에 바꿔줘야함.
	**************************************** */
	if (_b_fixed_action == false)
	{
		switch (_action)
		{
		case ACTION_ATTACK1:
		case ACTION_ATTACK2:
		case ACTION_ATTACK3:
			SetCharacterActionAttackSprite();
			_b_fixed_action = true;
			break;

		default:
			SetCharacterActionMoveStandSprite();
			break;
		}

		/* ***************************************
		클라에서 직접 컨트롤하는
		캐릭터는 서버에 패킷을 보내줌.
		**************************************** */
		if (this == g_player)
		{
			SendCharacterPacket();

			g_player_x = g_player->GetX();
			g_player_y = g_player->GetY();
		}

		_action_old = _action;
	}
	else
	{
		if (_b_create_effect)
		{
			CreateEffect();
		}
	}

	NextCharacterFrame();

	return _b_live;
}

void Character::InputCharacterAction(int action)
{
	_action = action;
}

/* ***************************************
ACTION_ATTACK123에 대한 스프라이트 정보를 업데이트
**************************************** */
void Character::SetCharacterActionAttackSprite()
{
	switch (_action)
	{
	case ACTION_ATTACK1:
		ChangeIntoCharacterActionAttack1Sprite();
		break;

	case ACTION_ATTACK2:
		ChangeIntoCharacterActionAttack2Sprite();
		break;

	case ACTION_ATTACK3:
		ChangeIntoCharacterActionAttack3Sprite();
		break;

	default:
		break;
	}
}

/* ***************************************
	Move & Stand 액션 정보 처리
**************************************** */
void Character::SetCharacterActionMoveStandSprite()
{
	const eDirection direction_old = _direction;
	eDirection direction;
	int action_old = _action_old;
	int action = _action;

	/* ***************************************
		좌표 계산
	**************************************** */
	{
		int x = GetX();
		int y = GetY();
		
		switch (action)
		{
		case ACTION_MOVE_DD:
			y += MOVE_DISTANCE_Y;

			if(y < MAP_MOVE_BOTTOM)
			{
				SetY(y);
			}

			break;

		case ACTION_MOVE_LD:
			x -= MOVE_DISTANCE_X;
			y += MOVE_DISTANCE_Y;

			if (x > MAP_MOVE_LEFT && y < MAP_MOVE_BOTTOM)
			{
				SetX(x);
				SetY(y);
			}

			break;

		case ACTION_MOVE_LL:
			x -= MOVE_DISTANCE_X;

			if (x > MAP_MOVE_LEFT)
			{
				SetX(x);
			}

			break;

		case ACTION_MOVE_LU:
			x -= MOVE_DISTANCE_X;
			y -= MOVE_DISTANCE_Y;

			if (x > MAP_MOVE_LEFT && y > MAP_MOVE_TOP)
			{
				SetX(x);
				SetY(y);
			}

			break;

		case ACTION_MOVE_UU:
			y -= MOVE_DISTANCE_Y;

			if (y > MAP_MOVE_TOP)
			{
				SetY(y);
			}

			break;

		case ACTION_MOVE_RU:
			x += MOVE_DISTANCE_X;
			y -= MOVE_DISTANCE_Y;
			
			if (x < MAP_MOVE_RIGHT && y > MAP_MOVE_TOP)
			{
				SetX(x);
				SetY(y);
			}
			
			break;

		case ACTION_MOVE_RR:
			x += MOVE_DISTANCE_X;

			if (x < MAP_MOVE_RIGHT)
			{
				SetX(x);
			}

			break;

		case ACTION_MOVE_RD:
			x += MOVE_DISTANCE_X;
			y += MOVE_DISTANCE_Y;

			if (x < MAP_MOVE_RIGHT && y < MAP_MOVE_BOTTOM)
			{
				SetX(x);
				SetY(y);
			}
			break;

		default:
			break;
		}
	}


	/* ***************************************
		액션 방향 계산
	**************************************** */
	{ 
		switch (action)
		{
		case ACTION_MOVE_LL:
		case ACTION_MOVE_LD:
		case ACTION_MOVE_LU:
			direction = eDirection::Left;
			break;

		case ACTION_MOVE_RR:
		case ACTION_MOVE_RD:
		case ACTION_MOVE_RU:
			direction = eDirection::Right;
			break;

		default:
			direction = direction_old;
			break;
		}
		_direction = direction;
	}

	/* ***************************************
		바로 이전의 액션이 ATTACK, STAND, MOVE인 경우로 나누어서 처리해준다.
		이전 입력과 현재 입력이 동일하면 따로 업데이트 없어야함.
		전체 구조 통일을 위해서 변수의 초기화시 처음 값을 받아두고 사용함.
	**************************************** */\
	if (action_old == ACTION_ATTACK1 || action_old == ACTION_ATTACK2 || action_old == ACTION_ATTACK3)
	{
		switch (action)
		{
		case ACTION_STAND:
			ChangeIntoCharacterActionStandSprite();
			break;

		default:
			ChangeIntoCharacterActionMoveSprite();
			break;
		}
	}
	else if (action_old == ACTION_STAND)
	{
		switch (action)
		{
		case ACTION_STAND:
			if(IsEndSprite())
			{
				ReturnStartSprite();
			}
			break;

		default:
			ChangeIntoCharacterActionMoveSprite();
			break;
		}
	}
	else
	{
		switch (action)
		{
		case ACTION_STAND:
			ChangeIntoCharacterActionStandSprite();
			break;
		
		default:
			if (direction == direction_old)
			{
				if (IsEndSprite())
				{
					ReturnStartSprite();
				}
			}
			else
			{
				ChangeIntoCharacterActionMoveSprite();
			}
			break;
		}
	}
}

void Character::ChangeIntoCharacterActionMoveSprite()
{
	int sprite_index = GetSpriteIndex();
	int sprite_start = GetSpriteStart();
	int sprite_end = GetSpriteEnd();
	int delay_per_frame = GetDelayPerFrame();
	int delay_count = GetDelayCount();
	SpriteDib* const sprite = GetSpriteDib();

	if (_direction == eDirection::Left)
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Move_L_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Move_L_End);
		delay_count = 0;
		delay_per_frame = DELAY_MOVE;
	}
	else
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Move_R_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Move_R_End);
		delay_count = 0;
		delay_per_frame = DELAY_MOVE;
	}

	ChangeSprite(sprite_index, sprite_start, sprite_end, delay_per_frame, delay_count);
}

void Character::ChangeIntoCharacterActionStandSprite()
{
	int sprite_index = GetSpriteIndex();
	int sprite_start = GetSpriteStart();
	int sprite_end = GetSpriteEnd();
	int delay_per_frame = GetDelayPerFrame();
	int delay_count = GetDelayCount();
	SpriteDib* const sprite = GetSpriteDib();

	if (_direction == eDirection::Left)
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Stand_L_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Stand_L_End);
		delay_count = 0;
		delay_per_frame = DELAY_STAND;
	}
	else
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Stand_R_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Stand_R_End);
		delay_count = 0;
		delay_per_frame = DELAY_STAND;
	}

	ChangeSprite(sprite_index, sprite_start, sprite_end, delay_per_frame, delay_count);
}

void Character::ChangeIntoCharacterActionAttack1Sprite()
{
	int sprite_start;
	int sprite_index;
	int sprite_end;
	int delay_per_frame;
	SpriteDib* const sprite = GetSpriteDib();

	if (_direction == eDirection::Left)
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack1_L_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack1_L_End);
		delay_per_frame = DELAY_ATTACK1;
	}
	else
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack1_R_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack1_R_End);
		delay_per_frame = DELAY_ATTACK1;
	}
	
	ChangeSprite(sprite_index, sprite_start, sprite_end, delay_per_frame, 0);
}

void Character::ChangeIntoCharacterActionAttack2Sprite()
{
	int sprite_start;
	int sprite_index;
	int sprite_end;
	int delay_per_frame;
	SpriteDib* const sprite = GetSpriteDib();

	if (_direction == eDirection::Left)
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack2_L_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack2_L_End);
		delay_per_frame = DELAY_ATTACK2;
	}
	else
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack2_R_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack2_R_End);
		delay_per_frame = DELAY_ATTACK2;
	}

	ChangeSprite(sprite_index, sprite_start, sprite_end, delay_per_frame, 0);
}

void Character::ChangeIntoCharacterActionAttack3Sprite()
{
	int sprite_start;
	int sprite_index;
	int sprite_end;
	int delay_per_frame;
	SpriteDib* const sprite = GetSpriteDib();

	if (_direction == eDirection::Left)
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack3_L_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack3_L_End);
		delay_per_frame = DELAY_ATTACK3;
	}
	else
	{
		sprite_start = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack3_R_01);
		sprite_index = sprite_start;
		sprite_end = sprite->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Attack3_R_End);
		delay_per_frame = DELAY_ATTACK3;
	}

	ChangeSprite(sprite_index, sprite_start, sprite_end, delay_per_frame, 0);
}

void Character::NextCharacterFrame()
{
	if (!ChangeCurrentFrameToNextFrame())
	{
		_b_fixed_action = false;

		/* ***************************************
		서버에서 받은 패킷이 '공격'일 때,
		연속적으로 '공격'을 받으면, 기존의 액션룰과는
		상관없이 다시 공격 액션 로직을 실행한다.
		**************************************** */
		if (this != g_player && (_action == ACTION_ATTACK1 || _action == ACTION_ATTACK2 || _action == ACTION_ATTACK3))
		{
			this->InputCharacterAction(ACTION_STAND);
		}
	}
}

void Character::SetDirection(unsigned char direction)
{
	_direction = static_cast<Character::eDirection>(direction);
}

void Character::SetHP(int HP)
{
	_HP = HP;
}

void Character::SetFixedAction(bool b_fixed)
{
	_b_fixed_action = b_fixed;
}

void Character::SetEffect(bool b_make)
{
	_b_create_effect = true;
}

void Character::CreateEffect()
{
	if (GetSpriteIndex() == (GetSpriteEnd() - 2) && GetDelayCount() == 0)
	{
		if (_direction == eDirection::Left)
		{
			g_object_list.push_back(new Effect(GetX() - 50, GetY() - 70,
				IBaseObject::eObjectType::Effect, GetSpriteDib()));

		}
		else
		{
			g_object_list.push_back(new Effect(GetX() + 50, GetY() - 70,
				IBaseObject::eObjectType::Effect, GetSpriteDib()));

		}

		_b_create_effect = false;
	}
}

void Character::SendCharacterPacket()
{
	if (!g_b_send)
	{
		return;
	}
	/* ***************************************
	PacketMoveStopCS 를 보내는 기준
	: 1. 이동 중에 공격
	2. 이동 중에 멈춤
	**************************************** */
	if (_action == ACTION_ATTACK1 || _action == ACTION_ATTACK2 || _action == ACTION_ATTACK3 || _action == ACTION_STAND)
	{
		if (_action_old != ACTION_ATTACK1 && _action_old != ACTION_ATTACK2 && _action_old != ACTION_ATTACK3 && _action_old != ACTION_STAND)
		{
			SendPacketMoveStopCS();
		}
	}

	/* ***************************************
	공격 & 이동 패킷
	**************************************** */
	switch (_action)
	{
	case ACTION_ATTACK1:
		SendPacketAttack1CS();
		break;

	case ACTION_ATTACK2:
		SendPacketAttack2CS();
		break;

	case ACTION_ATTACK3:
		SendPacketAttack3CS();
		break;

	default:
		break;
	}

	switch (_action)
	{
	case ACTION_MOVE_LL:
	case ACTION_MOVE_LU:
	case ACTION_MOVE_UU:
	case ACTION_MOVE_RU:
	case ACTION_MOVE_RR:
	case ACTION_MOVE_RD:
	case ACTION_MOVE_DD:
	case ACTION_MOVE_LD:
		if (_action_old != _action)
		{
			SendPacketMoveStartCS();
		}
		break;

	default:
		break;
	}

}

void Character::SendPacketMoveStopCS()
{
	SerializationBuffer buf;

	CreatePacketMoveStopCS(&buf, (unsigned char)_direction, GetX(), GetY());

	if (SendPacket(&buf) == false)
	{
		CleanUp();
	}

	SendProc();
}

void Character::SendPacketMoveStartCS()
{
	SerializationBuffer buf;

	CreatePacketMoveStartCS(&buf, (unsigned char)_action, GetX(), GetY());

	if (SendPacket(&buf) == false)
	{
		CleanUp();
	}

	SendProc();
}

void Character::SendPacketAttack1CS()
{
	SerializationBuffer buf;

	CreatePacketAttack1CS(&buf, (unsigned char)_direction, GetX(), GetY());

	if (SendPacket(&buf) == false)
	{
		CleanUp();
	}

	SendProc();
}

void Character::SendPacketAttack2CS()
{
	SerializationBuffer buf;

	CreatePacketAttack2CS(&buf, (unsigned char)_direction, GetX(), GetY());

	if (SendPacket(&buf) == false)
	{
		CleanUp();
	}

	SendProc();
}

void Character::SendPacketAttack3CS()
{
	SerializationBuffer buf;

	CreatePacketAttack3CS(&buf, (unsigned char)_direction, GetX(), GetY());

	if (SendPacket(&buf) == false)
	{
		CleanUp();
	}

	SendProc();
}