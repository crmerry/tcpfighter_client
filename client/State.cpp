#include "stdafx.h"
#include "AllOfDefine.h"
#include "IBaseObject.h"
#include "Character.h"
#include "State.h"

void MoveUpRightState::Excute(IBaseObject* npc)
{
	int x = npc->GetX();
	int y = npc->GetY();
	int random = rand() % 100;

	if (y > MAP_MOVE_TOP)
	{
		static_cast<Character*>(npc)->InputCharacterAction(ACTION_MOVE_UU);
	}

	if (y <= MAP_MOVE_TOP)
	{
		static_cast<Character*>(npc)->InputCharacterAction(ACTION_MOVE_RR);
	}

	if (random > 96)
	{
		if (random == 97)
		{
			static_cast<Character*>(npc)->InputCharacterAction(ACTION_ATTACK1);
		}
		if (random == 98)
		{
			static_cast<Character*>(npc)->InputCharacterAction(ACTION_ATTACK2);
		}
		if (random == 99)
		{
			static_cast<Character*>(npc)->InputCharacterAction(ACTION_ATTACK3);
		}
	}
}

void MoveUpRightState::Exit()
{
}

void MoveUpRightState::Enter(IBaseObject * npc)
{

}

MoveUpRightState* MoveUpRightState::GetInstance()
{
	static MoveUpRightState state;

	return &state;
}

void MoveDownLeftState::Excute(IBaseObject * npc)
{
	int x = npc->GetX();
	int y = npc->GetY();
	int random = rand() % 100;

	if (y < MAP_MOVE_BOTTOM)
	{
		static_cast<Character*>(npc)->InputCharacterAction(ACTION_MOVE_DD);
	}

	if (y >= MAP_MOVE_BOTTOM)
	{
		static_cast<Character*>(npc)->InputCharacterAction(ACTION_MOVE_LL);
	}

	if (random > 96)
	{
		if (random == 97)
		{
			static_cast<Character*>(npc)->InputCharacterAction(ACTION_ATTACK1);
		}
		if (random == 98)
		{
			static_cast<Character*>(npc)->InputCharacterAction(ACTION_ATTACK2);
		}
		if (random == 99)
		{
			static_cast<Character*>(npc)->InputCharacterAction(ACTION_ATTACK3);
		}
	}
}

// TODO:
// 흠.. Eixt Excute Enter가 뭔가 표준 적인 형태인거 같은데
// 딱히 애매하네 쓰기가
void MoveDownLeftState::Exit()
{
}

void MoveDownLeftState::Enter(IBaseObject * npc)
{
}

MoveDownLeftState* MoveDownLeftState::GetInstance()
{
	static MoveDownLeftState state;
	
	return &state;
}
