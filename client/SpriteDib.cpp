#include "stdafx.h"
#include "SpriteDib.h"

SpriteDib::SpriteDib(int sprite_count_max, DWORD transparent_color_key)
	: _sprite_count_max(sprite_count_max), _transparent_color_key(transparent_color_key)
{	
	_sprite_data = (SpriteInfo*)malloc(sizeof(SpriteInfo)*sprite_count_max);
	SpriteInfo* sprite_data = _sprite_data;

	for (int i = 0; i < sprite_count_max; i++)
	{
		sprite_data[i]._is_used = false;
	}

	_sprite_to_index_table = (int**)malloc(sizeof(int*)*sprite_count_max);
	
	for (int index = 0; index < sprite_count_max; index++)
	{
		_sprite_to_index_table[index] = (int*)malloc(sizeof(int));
	}
}

SpriteDib::~SpriteDib()
{
	free(_sprite_data);
}

//*******************************************
// 스프라이트 정보를 BMP 파일로부터 읽음
// 이미지 데이터와 함께 인덱스를 지정해 sprite_data에 저장
//*******************************************
BOOL SpriteDib::LoadDibSprite(int sprite_index, eSpriteName sprite_name, wchar_t* file_name, int x_center, int y_center)
{
	SpriteInfo& sprite_data = _sprite_data[sprite_index];

	if (sprite_data._is_used || sprite_index > _sprite_count_max)
	{
		return false;
	}

	FILE* fp;
	_wfopen_s(&fp, file_name, L"rb");

	BITMAPFILEHEADER bitmap_file_header;
	size_t error = fread_s(&bitmap_file_header, sizeof(bitmap_file_header), sizeof(bitmap_file_header), 1, fp);	
	
	if (error != 1)
	{
		return false;
	}

	BITMAPINFOHEADER bitmap_info_header;
	error = fread_s(&bitmap_info_header, sizeof(bitmap_info_header), sizeof(bitmap_info_header), 1, fp);
	
	if (error != 1)
	{
		return false;
	}

	int width = bitmap_info_header.biWidth;
	int height = bitmap_info_header.biHeight;
	int pitch = width * bitmap_info_header.biBitCount / 8;
	pitch = (pitch + 3) & ~3;

	BYTE* src_image = new BYTE[pitch * height];
	sprite_data._image = new BYTE[pitch * height];
	BYTE* dest_buffer = sprite_data._image;

	error = fread_s(src_image, sizeof(BYTE) * pitch * height, sizeof(BYTE) * pitch * height, 1, fp);
	BYTE* src_buffer = src_image;
	src_buffer += pitch * (height - 1);

	if (error != 1)
	{
		return false;
	}

	for (int i = 0; i < height; i++)
	{
		// widt * 4 대신에, pitch를 썼는데
		// pitch로 하면 정렬을 위한 쓰레기 값까지 복사되는 것.
		memcpy_s(dest_buffer, width * 4, src_buffer, width * 4);

		src_buffer -= pitch;
		dest_buffer += pitch;
	}

	sprite_data._height = height;
	sprite_data._width = width;
	sprite_data._pitch = pitch;
	sprite_data._x_center = x_center;
	sprite_data._y_center = y_center;
	sprite_data._is_used = true;
	sprite_data.sprite_name = sprite_name;

	delete[] src_image;

	return true;
}

//*******************************************
//스프라이트 정보를 읽어 왔을 때, 
//스프라이트 이름(enum class)으로부터 스프라이트 인덱스를 얻기 위해 
//최초에 GmaeInitialize부분에서 한 번 호출해줌.
//*******************************************
void SpriteDib::InitializeSpriteToIndexTable()
{
	int sprite_count_max = _sprite_count_max;
	
	for (int index = 0; index < sprite_count_max; index++)
	{
		_sprite_to_index_table[static_cast<int>(_sprite_data[index].sprite_name)][0] = index;
	}
}

void SpriteDib::ReleaseSprite(int sprite_index)
{
}

//*******************************************
// draw_len 은 width를 기준으로 draw_len %만큼.
// 즉, 기본 값이 100이므로 항상 100퍼센트 출력
// 만약 50이라면, 50퍼센트만큼 자르는 건데, 실제 축소하는게 아니고,
// 잘라서 50퍼센트만큼만 출력.
// dest에 이미지를 그려준다.
//*******************************************
void SpriteDib::DrawSprite(int sprite_index, int sprite_x, int sprite_y, BYTE * dest, int width, int height, int pitch, bool is_alpha, int draw_len)
{
	if (sprite_index > _sprite_count_max)
	{
		return;
	}

	SpriteInfo& sprite_data = _sprite_data[sprite_index];
	
	if (sprite_data._is_used == false)
	{
		return;
	}

	float base_draw_length = (float)draw_len / 100;

	BYTE* image = sprite_data._image;

	int s_width = sprite_data._width;
	int s_pitch = sprite_data._pitch;
	int s_height = sprite_data._height;

	int x_center = sprite_data._x_center;
	int y_center = sprite_data._y_center;

	int top = sprite_y - y_center;
	int left = sprite_x - x_center;
	int right = sprite_x + x_center;
	int bottom = sprite_y; // TODO : sprite_y로 하면 밑에 그림이 아주 살짝 잘리게 된다. 이 부분이 조정이 필요할 것.

	int x_start, x_end, y_start, y_end;
	int x_image, y_image;

	/* ***************************************
	클리핑.	
	**************************************** */
	if (top < 0)
	{
		y_start = 0;
		y_image = (-1)*top;
	}
	else
	{
		y_start = top;
		y_image = 0;
	}
		
	if (left < 0)
	{
		x_start = 0;
		x_image = (-1)*left;
	}
	else
	{
		x_start = left;
		x_image = 0;
	}
	
	if (right > width)
	{
		x_end = width;
	}
	else
	{
		x_end = right;
	}

	if (bottom > height)
	{
		y_end = height;
	}
	else
	{
		y_end = bottom;
	}

	float draw_width = (float)(x_end - x_start);
	draw_width = draw_width * base_draw_length;
	
	x_end = (int)((float)x_start + draw_width);
	
	dest = dest + x_start*4 + y_start * pitch;
	
	image = image + x_image*4 + y_image * s_pitch;

	DWORD* src_ptr = (DWORD*)image;
	DWORD* dst_ptr = (DWORD*)dest; 

	if (is_alpha == false)
	{
		for (int row_index = y_start; row_index < y_end; row_index++)
		{
			src_ptr = (DWORD*)image;
			dst_ptr = (DWORD*)dest;

			for (int col_index = x_start; col_index < x_end; col_index++)
			{
				if ((*src_ptr & 0x00FFFFFF) != 0x00FFFFFF)
				{
					*dst_ptr = *src_ptr;
				}

				src_ptr++;
				dst_ptr++;
			}

			image += s_pitch;
			dest += pitch;
		}
	}
	else
	{
		for (int row_index = y_start; row_index < y_end; row_index++)
		{
			src_ptr = (DWORD*)image;
			dst_ptr = (DWORD*)dest;

			for (int col_index = x_start; col_index < x_end; col_index++)
			{
				if ((*src_ptr & 0x00FFFFFF) != 0x00FFFFFF)
				{
					*((BYTE*)dst_ptr + 0) = *((BYTE*)src_ptr + 0) / 2 + *((BYTE*)dst_ptr + 0) / 2;
					*((BYTE*)dst_ptr + 1) = *((BYTE*)src_ptr + 1) / 2 + *((BYTE*)dst_ptr + 1) / 2;
					*((BYTE*)dst_ptr + 2) = *((BYTE*)src_ptr + 2) / 2 + *((BYTE*)dst_ptr + 2) / 2;
				}

				src_ptr++;
				dst_ptr++;
			}

			image += s_pitch;
			dest += pitch;
		}
	}
}

//*******************************************
// 일단은 맵을 출력해주기 위한...
// 만약 맵을 따로 클래스로 만들어준다면 이 함수를
// 제외하고 그 클래스에 가야할 것.
//*******************************************
void SpriteDib::DrawImage(int sprite_index, int sprite_x, int sprite_y, BYTE * dest, int width, int height, int pitch, int draw_len)
{
	SpriteInfo& sprite_data = _sprite_data[sprite_index];

	BYTE* image = sprite_data._image;
	int s_width = sprite_data._width;
	int s_pitch = sprite_data._pitch;
	int s_height = sprite_data._height;
	
	for (int row_index = 0; row_index < height; row_index++)
	{
		memcpy_s(dest, pitch, image, s_pitch);

		image += s_pitch;
		dest += pitch;
	}
}

//*******************************************
//InitializeSpriteToIndexTable로 초기화한 데이터를
//스프라이트 이름과 대응되는 인덱스를 찾아준다.
//*******************************************
int SpriteDib::GetSpriteIndexBySpriteName(eSpriteName sprite_name)
{
	return _sprite_to_index_table[static_cast<int>(sprite_name)][0];
}