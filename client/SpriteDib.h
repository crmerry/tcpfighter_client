#pragma once

class SpriteDib
{
public:
	enum class eSpriteName
	{
		Attack1_L_01,
		Attack1_L_02,
		Attack1_L_03,
		Attack1_L_04,
		Attack1_L_End = Attack1_L_04,

		Attack1_R_01,
		Attack1_R_02,
		Attack1_R_03,
		Attack1_R_04,
		Attack1_R_End = Attack1_R_04,

		Attack2_L_01,
		Attack2_L_02,
		Attack2_L_03,
		Attack2_L_04,
		Attack2_L_End = Attack2_L_04,

		Attack2_R_01,
		Attack2_R_02,
		Attack2_R_03,
		Attack2_R_04,
		Attack2_R_End = Attack2_R_04,

		Attack3_L_01,
		Attack3_L_02,
		Attack3_L_03,
		Attack3_L_04,
		Attack3_L_05,
		Attack3_L_06,
		Attack3_L_End = Attack3_L_06,

		Attack3_R_01,
		Attack3_R_02,
		Attack3_R_03,
		Attack3_R_04,
		Attack3_R_05,
		Attack3_R_06,
		Attack3_R_End = Attack3_R_06,

		Move_L_01,
		Move_L_02,
		Move_L_03,
		Move_L_04,
		Move_L_05,
		Move_L_06,
		Move_L_07,
		Move_L_08,
		Move_L_09,
		Move_L_10,
		Move_L_11,
		Move_L_12,
		Move_L_End = Move_L_12,

		Move_R_01,
		Move_R_02,
		Move_R_03,
		Move_R_04,
		Move_R_05,
		Move_R_06,
		Move_R_07,
		Move_R_08,
		Move_R_09,
		Move_R_10,
		Move_R_11,
		Move_R_12,
		Move_R_End = Move_R_12,

		Stand_L_01,
		Stand_L_02,
		Stand_L_03,
		Stand_L_End = Stand_L_03,

		Stand_R_01,
		Stand_R_02,
		Stand_R_03,
		Stand_R_End = Stand_R_03,

		xSpark_1,
		xSpark_2,
		xSpark_3,
		xSpark_4,
		xSpark_End = xSpark_4,

		Shadow_1,
		Shadow_End = Shadow_1,

		HPgauage_1,
		HPgauage_End = HPgauage_1,

		Map,
	};

	typedef struct SPRITE_INFO
	{
		bool	_is_used;
		BYTE*	_image;
		int		_width;
		int		_height;
		int		_pitch;			
		int		_x_center;			
		int		_y_center;	
		eSpriteName sprite_name;
	}SpriteInfo;

	SpriteDib(int sprite_count_max, DWORD transparent_color_key);
	virtual ~SpriteDib();
	
	BOOL LoadDibSprite(int sprite_index, eSpriteName sprite_name, wchar_t* file_name, int x_center, int y_center);
	void InitializeSpriteToIndexTable();
	void ReleaseSprite(int sprite_index);
	void DrawSprite(int sprite_index, int sprite_x, int sprite_y, BYTE* dest, int width,
		int height, int pitch, bool is_alpha = false ,int draw_len = 100);
	void DrawImage(int sprite_index, int sprite_x, int sprite_y, BYTE* dest, int width,
		int height, int pitch, int draw_len = 100);
	int GetSpriteIndexBySpriteName(eSpriteName sprite_name);
protected:
	SpriteInfo*	_sprite_data;
	int**		_sprite_to_index_table;
	int			_sprite_count_max;
	DWORD		_transparent_color_key;
};