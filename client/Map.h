#pragma once

class SpriteDib;

class Map
{
public:
	static Map* GetInstance(SpriteDib* sprite_dib);
	void DrawMap(int character_x, int character_y, BYTE* dest, int width, int height, int pitch);
	void ChangeMap(int map_index);

private:
	Map(SpriteDib* sprite_dib, int map_index);
	~Map();

private:
	SpriteDib*	_sprite_dib;
	int			_map_index;
	char		_tile_map[20][20];
};

