#pragma once

/* ***************************************
	[RingBuffer]

	Empty:            Full:
	ㅁㅁㅁㅁㅁㅁㅁ    ㅁㅁㅁㅁㅁㅁㅁ
	 F				     F
	 R		           R
    
	Enqueue
	ㅇㅁㅁㅁㅁㅁㅁ
	 F
	   R

	Enqueue
	ㅇㅇㅁㅁㅁㅁㅁ
	 F
	     R

	Dequeue
	ㅁㅇㅁㅁㅁㅁㅁ
	   F
	     R

	GetWirteBufferPtr : 마지막 인덱스를 반환
	ㅁㅁㅁㅁㅁㅁㅁ
	     F       R

	GetReadBufferPtr : 마지막 인덱스를 반환
	ㅁㅁㅁㅁㅁㅁㅁ
	     R       F

    GetWritableSizeAtOnce : size - 마지막 인덱스반환(1)
	ㅁㅁㅁㅁㅁㅁㅁ
	     F       R
	
	GetWritableSizeAtOnce : R부터 마지막 인덱스까지의 사이즈를 반환
	ㅁㅁㅁㅁㅁㅁㅁ
	F       R

	GetReadableSizeAtOnce : size - 마지막 인덱스 반환(1)
	ㅁㅁㅁㅁㅁㅁㅁ
	     R	     F

	GetReadableSizeAtOnce : F이후부터 마지막 R까지의 사이즈를 반환
	ㅁㅁㅁㅁㅁㅁㅁ
	F			 R
**************************************** */
class RingBuffer
{
public:
	enum
	{
		DEFAULT_BUFFER_SIZE = 100,
	};

	RingBuffer(int buffer_size = DEFAULT_BUFFER_SIZE);
	~RingBuffer();

	int GetBufferSize();
	int GetUsedSize();
	int GetFreeSize();
	int GetWritableSizeAtOnce();
	int GetReadableSizeAtOnce();
	char* GetBufferPtr();
	
	/* ***************************************
		 GetReadBufferPtr, GetWriteBufferPtr은
		rear와 front에 연산을 하여 _buffer의
		특정 인덱스의 주소 값을 받는다.
		 호출 후에 MoveFront...MoveRear..
		을 호출해줘야함.
	**************************************** */
	char* GetWriteBufferPtr();
	char* GetReadBufferPtr();
	void MoveFrontAfterRead(int size);
	void MoveRearAfterWrite(int size);

	int Enqueue(char* data, int write_size);
	int Dequeue(char* data, int read_size);
	int Peek(char* data, int read_size);

	void ClearBuffer();
private:
	char* _buffer;
	int _size;
	int _front;
	int _rear;
};