#include "stdafx.h"
#include "AllOfDefine.h"
#include "IBaseObject.h"
#include "SpriteDib.h"
#include "State.h"

/* ***************************************
TODO:디렉션에 상관없이 L1만 하므로
나중에 바꿔줘야함.
**************************************** */
IBaseObject::IBaseObject(int x, int y, eObjectType type, SpriteDib* sprite_dib)
	:_x(x), _y(y), _type(type), _sprite_dib(sprite_dib)
{
	_sprite_index = sprite_dib->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Stand_L_01);
	_sprite_start = sprite_dib->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Stand_L_01);
	_sprite_end = sprite_dib->GetSpriteIndexBySpriteName(SpriteDib::eSpriteName::Stand_L_End);
	_delay_per_frame = DELAY_STAND;
	_delay_count = 0;
	_is_end_sprite = false;
}

IBaseObject::~IBaseObject()
{

}

int IBaseObject::GetX() const
{
	return _x;
}

int IBaseObject::GetY() const
{
	return _y;
}

int IBaseObject::GetSpriteIndex() const
{
	return _sprite_index;
}

int IBaseObject::GetSpriteStart() const
{
	return _sprite_start;
}

int IBaseObject::GetSpriteEnd() const
{
	return _sprite_end;
}

int IBaseObject::GetDelayCount() const
{
	return _delay_count;
}

int IBaseObject::GetDelayPerFrame() const
{
	return _delay_per_frame;
}

IBaseObject::eObjectType IBaseObject::GetType() const
{
	return _type;
}

bool IBaseObject::IsEndSprite() const
{
	return _is_end_sprite;
}

void IBaseObject::SetX(int x)
{
	_x = x;
}

void IBaseObject::SetY(int y)
{
	_y = y;
}

void IBaseObject::SetSpriteIndex(int index)
{
	_sprite_index = index;
}

void IBaseObject::SetSpriteStart(int sprite_start)
{
	_sprite_start = sprite_start;
}

void IBaseObject::SetSpriteEnd(int sprite_end)
{
	_sprite_end = sprite_end;
}

void IBaseObject::SetDelayPerFrame(int delay_max)
{
	_delay_per_frame = delay_max;
}

void IBaseObject::SetDelayCount(int delay_count)
{
	_delay_count = delay_count;
}

void IBaseObject::ChangeSprite(int index, int start, int end, int delay_per_frame, int delay_count)
{
	_sprite_index = index;
	_sprite_start = start;
	_sprite_end = end;
	_delay_per_frame = delay_per_frame;
	_delay_count = delay_count;
}

void IBaseObject::ReturnStartSprite()
{
	_sprite_index = _sprite_start;
}

//*******************************************
// start -> end까지 그려 준다.
// end가 아니라면 true, end라면 false를 리턴해준다.
//*******************************************
bool IBaseObject::ChangeCurrentFrameToNextFrame()
{
	const int sprite_start = _sprite_start;
	const int sprite_end = _sprite_end;
	const int delay_max = _delay_per_frame;

	int delay_count = _delay_count;
	int sprite_index = _sprite_index;

	_is_end_sprite = false;

	if (sprite_index >= sprite_end && delay_count >= delay_max)
	{
		_delay_count = 0;
		_is_end_sprite = true;

		return false;
	}

	if (delay_count >= delay_max)
	{
		delay_count = -1;
		sprite_index += 1;
	}

	delay_count++;
	_delay_count = delay_count;
	_sprite_index = sprite_index;

	return true;
}

SpriteDib* IBaseObject::GetSpriteDib() const
{
	return _sprite_dib;
}